-- LUALOCALS < ---------------------------------------------------------
local string
    = string
local string_sub
    = string.sub
-- LUALOCALS > ---------------------------------------------------------

-- Define the chars that are allowed to be used in ascii art tables.
-- The size of the alphabet limits the maximum number of unique node
-- definition types that can be used in a single schematic.
local alphabet = {}
do
	-- Only use printable 7-bit ascii characters that don't
	-- require escaping in a lua string, for compatibility.
	local chars = " .abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	.. "0123456789,/;'[]`-=<>?:{}|~!@#$%^&*()_+"
	for i = 1, #chars do
		alphabet[#alphabet + 1] = string_sub(chars, i, i)
	end
end

return alphabet
