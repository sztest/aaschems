-- LUALOCALS < ---------------------------------------------------------
local dofile, ipairs, minetest, pairs, rawget, rawset, string, table
    = dofile, ipairs, minetest, pairs, rawget, rawset, string, table
local string_format, table_concat
    = string.format, table.concat
-- LUALOCALS > ---------------------------------------------------------

local alphabet = dofile(minetest.get_modpath(
		minetest.get_current_modname()) .. "/alphabet.lua")

-- Dump an asciiart lua table in a format optimized for editing.
-- Use loadstring/dofile/etc to reverse.
local function asciiart_to_lua(aa)
	local t = {
		"return {",
		"\tnodes = {",
	}
	for _, k in pairs(alphabet) do
		local v = aa.nodes[k]
		if v then
			local u = {}
			if v.name then u[#u + 1] = string_format("name = %q", v.name) end
			if v.param2 then u[#u + 1] = string_format("param2 = %d", v.param2) end
			if v.prob then u[#u + 1] = string_format("prob = %d", v.prob) end
			if v.force then u[#u + 1] = "force = true" end
			t[#t + 1] = string_format("\t\t[%q] = {%s},", k, table_concat(u, ", "))
		end
	end
	t[#t + 1] = "\t},"
	if aa.sliceprob then
		t[#t + 1] = "\tsliceprob = {"
		for y = 0, aa.size.y - 1 do
			local p = aa.sliceprob[y]
			if p then
				t[#t + 1] = string_format("\t\t[%d] = %d,", y, p)
			end
		end
		t[#t + 1] = "\t},"
	end
	t[#t + 1] = "\tlayers = {"
	for _, layer in ipairs(aa.layers) do
		t[#t + 1] = "\t\t{"
		local row = 1
		for _, row in ipairs(layer) do
			t[#t + 1] = string_format("\t\t\t%q,", row)
		end
		t[#t + 1] = "\t\t},"
	end
	t[#t + 1] = "\t},"
	t[#t + 1] = "}"
	return table_concat(t, "\n")
end

------------------------------------------------------------------------

local modname = minetest.get_current_modname()
local api = rawget(_G, modname) or {}
rawset(_G, modname, api)

api.asciiart_to_lua = asciiart_to_lua
