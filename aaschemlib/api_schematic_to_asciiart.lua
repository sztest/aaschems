-- LUALOCALS < ---------------------------------------------------------
local dofile, error, minetest, next, pairs, rawget, rawset, string
    = dofile, error, minetest, next, pairs, rawget, rawset, string
local string_sub
    = string.sub
-- LUALOCALS > ---------------------------------------------------------

local alphabet = dofile(minetest.get_modpath(
		minetest.get_current_modname()) .. "/alphabet.lua")

-- Convert a schematic lua table (in the format of minetest.read_schematic)
-- into an asciiart lua table. Use asciiart_to_schematic to reverse.
local function schematic_to_asciiart(mts)
	local nodes = {
		-- Reserve space for "void" (don't overwrite existing node)
		[" "] = {prob = 0},
		-- Reserve period for air (just remove existing node)
		["."] = {name = "air"},
	}
	local alphaidx = 3
	local usage = {}

	local layers = {}
	local function set(x, y, z, s)
		x, y, z = x + 1, y + 1, mts.size.z - z
		local l = layers[y]
		if not l then
			l = {}
			layers[y] = l
		end
		local r = l[z] or ""
		while (#r < x) do r = r .. " " end
		l[z] = string_sub(r, 1, x - 1)
		.. s .. string_sub(r, x + 2)
		usage[s] = true
	end

	local i = 1
	for z = 0, mts.size.z - 1 do
		for y = 0, mts.size.y - 1 do
			for x = 0, mts.size.x - 1 do
				local v = mts.data[i]
				i = i + 1
				local t = {
					name = v.name,
					param2 = v.param2,
					prob = v.prob,
					force = v.force_place or nil
				}
				if t.param2 == 0 then t.param2 = nil end
				if t.prob and t.prob >= 254 then t.prob = nil end
				if t.prob and t.prob <= 0 then
					t.name = nil
					t.param2 = nil
				end
				local added
				for nk, nv in pairs(nodes) do
					if nv.name == t.name and nv.param2 == t.param2
					and nv.prob == nv.prob and nv.force == t.force then
						set(x, y, z, nk)
						added = true
						break
					end
				end
				if not added then
					local s = alphabet[alphaidx]
					alphaidx = alphaidx + 1
					if not s then error("too many unique nodes for chars") end
					nodes[s] = t
					set(x, y, z, s)
				end
			end
		end
	end

	local t = {}
	for k, v in pairs(mts) do t[k] = v end
	t.size = nil
	t.data = nil
	t.layers = layers
	t.nodes = {}
	for k in pairs(usage) do
		t.nodes[k] = nodes[k]
	end
	if t.yslice_prob and next(t.yslice_prob) then
		local d = {}
		for _, v in pairs(t.yslice_prob) do
			d[v.ypos] = v.prob
		end
		t.sliceprob = d
	end
	t.yslice_prob = nil

	return t
end

------------------------------------------------------------------------

local modname = minetest.get_current_modname()
local api = rawget(_G, modname) or {}
rawset(_G, modname, api)

api.schematic_to_asciiart = schematic_to_asciiart
