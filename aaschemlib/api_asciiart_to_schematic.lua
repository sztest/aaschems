-- LUALOCALS < ---------------------------------------------------------
local error, ipairs, minetest, pairs, rawget, rawset
    = error, ipairs, minetest, pairs, rawget, rawset
-- LUALOCALS > ---------------------------------------------------------

-- Convert an asciiart lua table back into a minetest schematic one,
-- reversing schematic_to_asciiart.
local function asciiart_to_schematic(aa)
	local schem = {
		size = {y = #aa.layers},
		data = {},
		yslice_prob = {},
	}
	for y, ys in ipairs(aa.layers) do
		if schem.size.z and schem.size.z ~= #ys then
			error("inconsistent z size")
		end
		schem.size.z = #ys
		for z, zs in ipairs(ys) do
			if schem.size.x and schem.size.x ~= #zs then
				error("inconsistent x size")
			end
			schem.size.x = #zs
			for x = 1, zs:len() do
				local node = aa.nodes[zs:sub(x, x)]
				schem.data[(schem.size.z - z) * schem.size.x * schem.size.y
				+ (y - 1) * schem.size.x + x] = node
			end
		end
	end
	for k, v in pairs(aa.sliceprob or {}) do
		yslice_prob[#yslice_prob + 1] = {ypos = k, prob = v}
	end
	return schem
end

------------------------------------------------------------------------

local modname = minetest.get_current_modname()
local api = rawget(_G, modname) or {}
rawset(_G, modname, api)

api.asciiart_to_schematic = asciiart_to_schematic
