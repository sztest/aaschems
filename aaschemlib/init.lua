-- LUALOCALS < ---------------------------------------------------------
local dofile, ipairs, minetest
    = dofile, ipairs, minetest
-- LUALOCALS > ---------------------------------------------------------

local modpath = minetest.get_modpath(minetest.get_current_modname())
for _, fn in ipairs({
		"api_asciiart_to_lua",
		"api_asciiart_to_schematic",
		"api_schematic_to_asciiart",
	}) do dofile(modpath .. "/" .. fn .. ".lua") end
