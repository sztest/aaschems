-- LUALOCALS < ---------------------------------------------------------
local dofile, ipairs, minetest
    = dofile, ipairs, minetest
-- LUALOCALS > ---------------------------------------------------------

local modpath = minetest.get_modpath(minetest.get_current_modname())
for _, fn in ipairs({
		"api_fileconv",
		"cmd_mts2aa",
		"cmd_aa2mts",
	}) do dofile(modpath .. "/" .. fn .. ".lua") end
