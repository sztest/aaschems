-- LUALOCALS < ---------------------------------------------------------
local io, minetest, pairs, pcall, rawget, rawset, string, unpack
    = io, minetest, pairs, pcall, rawget, rawset, string, unpack
local io_open, string_format, string_sub
    = io.open, string.format, string.sub
-- LUALOCALS > ---------------------------------------------------------

local function fileconv(func)
	return function(_, param)
		local src, dest = unpack(param:split(" "))
		if (not src) or (src == "") or (not dest) or (dest == "") then
			return false, "incorrect params"
		end

		-- Input pathnames can be prefixed with modname: to make the path
		-- relative to a loaded mod. Input pathnames can be prefixed with
		-- just a bare colon to make the path relative to the loaded world.
		for _, n in pairs(minetest.get_modnames()) do
			if string_sub(src, 1, #n + 1) == n .. ":" then
				src = minetest.get_modpath(n) .. "/" .. string_sub(src, #n + 2)
			end
		end
		if string_sub(src, 1, 1) == ":" then
			src = minetest.get_worldpath() .. "/" .. string_sub(src, 2)
		end

		-- Output pathnames must always be relative to the world path.
		dest = minetest.get_worldpath() .. "/" .. dest

		-- Run the conversion process and check for errors.
		local okay, result = pcall(func, src, dest)
		if not okay then
			return false, string_format("error convertin %q to %q: %s", src, dest, result)
		end

		-- Write the output
		local f = io_open(dest, "wb")
		if not f then
			return false, string_format("failed to open %q for writing", dest)
		end
		f:write(result)
		f:close()
		return true, string_format("wrote %d bytes to %q", #result, dest)
	end
end

------------------------------------------------------------------------

local modname = minetest.get_current_modname()
local api = rawget(_G, modname) or {}
rawset(_G, modname, api)

api.fileconv = fileconv
