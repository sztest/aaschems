-- LUALOCALS < ---------------------------------------------------------
local minetest, rawget
    = minetest, rawget
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = rawget(_G, modname)

minetest.register_chatcommand("mts2aa", {
		privs = {server = true},
		description = "Convert MTS schematic to Lua AA",
		params = "modname:path/to/file.mts output.lua",
		func = api.fileconv(function(src, dest)
				return aaschemlib.asciiart_to_lua(
					aaschemlib.schematic_to_asciiart(
						minetest.read_schematic(src, {
								write_yslice_prob = "low"
							})
					)
				)
			end)
	})