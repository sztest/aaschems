-- LUALOCALS < ---------------------------------------------------------
local dofile, minetest, rawget
    = dofile, minetest, rawget
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = rawget(_G, modname)

minetest.register_chatcommand("aa2mts", {
		privs = {server = true},
		description = "Convert Lua AA schematic to MTS (WARNING: Arbitrary Code Execution)",
		params = "modname:path/to/file.lua output.mts",
		func = api.fileconv(function(src, dest)
				return minetest.serialize_schematic(
					aaschemlib.asciiart_to_schematic(dofile(src)),
					"mts", {})
			end)
	})
